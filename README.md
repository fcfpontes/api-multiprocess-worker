# Multiprocess worker API

This is a simple example of how to use Python multiprocessing module to create a web APi with simple asynchronous worker capabilities.

Each API instance should have one worker to echo the request to another endpoint after a delay.

# Usage

The project uses pipenv as its management tool. Just run `pipenv install` if pipenv is installed or run it after `pip install pipenv`.

After that, just run gunicorn against the two modules' API and that's it.

`gunicorn -b 0.0.0.0:8081 echo_api:api`

`gunicorn -b 0.0.0.0:8080 worker_api:api`
  