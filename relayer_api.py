from time import sleep
from multiprocessing import Process, Queue

import falcon
import requests

DELAY = 1

q = Queue()


def worker_task():
    while True:
        data = q.get()
        sleep(DELAY)
        requests.post('http://localhost:8081', json=data)


class RootResource:
    def on_post(self, req, resp):
        data = req.media
        print(f'Received: {data}')
        q.put(data)
        resp.status = falcon.HTTP_ACCEPTED


def create_api():
    p = Process(target=worker_task)
    p.daemon = True
    p.start()

    api = falcon.API()
    api.add_route('/', RootResource())

    return api

api = create_api()