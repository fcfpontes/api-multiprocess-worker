from time import sleep

import falcon


class EchoResource:
    def on_post(self, req, resp):
        sleep(.5)
        print(req.media.get('test')[-1], )


def create_api():
    api = falcon.API()
    api.add_route('/', EchoResource())

    return api


api = create_api()
