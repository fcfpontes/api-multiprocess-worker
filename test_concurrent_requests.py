from multiprocessing import Process, Pool
import requests

def req(i):
    try:
        r = requests.post('http://localhost:8081', json={'test': list(range(i))})
        return r.status_code
    except requests.exceptions.ConnectionError as e:
        return 'timed out'

p = Pool(1000)
in_list = list(range(1, 100000))
result_list = p.map(req, in_list)
print(result_list.count('timed out'))
